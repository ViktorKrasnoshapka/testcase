package main

import (
    "fmt"
    "time"
    "encoding/json"
)

type NumberInfo struct {
    Index int
    Value int
}

var CurrentNumberInfo = NumberInfo{}

var attemptLeft = 3
var correctAnswers = 0

func main() {
    for n := 0; n <= 10; n++ {
        curNum := fibonacci(n)
        CurrentNumberInfo.Value = curNum
        CurrentNumberInfo.Index++

        c1 := make(chan int)
        go func() {
            var input int
            fmt.Scanln(&input)
            c1 <- input
        }()

        var t int
        select {
        case t = <-c1:
            if t != curNum {
                j, _ := json.Marshal(CurrentNumberInfo)
                println(string(j))
                attemptLeft--

                if attemptLeft == 0 {
                    return
                }
            }
            correctAnswers ++
            if correctAnswers == 10 {
                return
            }
        case <-time.After(10 * time.Second):
            j, _ := json.Marshal(CurrentNumberInfo)
            println(string(j))
            attemptLeft--

            if attemptLeft == 0 {
                return
            }
        }
    }
}

func fibonacci(n int) int {
    if n == 0 {
        return 0
    } else if n < 3 {
        return 1
    } else {
        return fibonacci(n-1) + fibonacci(n-2)
    }
}